const path = require('path');
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const publicPath = path.join(__dirname, '..', 'public');
app.use(express.static(publicPath));
app.listen(port, () => {
   console.log(`Server is up on port ${port}!`);
});
5. Make sure your index.html file is served, in case the user requests a resource currently not in the public folder.
const path = require('path');
const express = require('express');
const app = express();
const publicPath = path.join(__dirname, 'dist/tcd0201-cms');
const port = process.env.PORT || 3000;
app.use(express.static(publicPath));
app.get('*', (req, res) => {
   res.sendFile(path.join(publicPath, 'dist/tcd0201-cms/index.html'));
});
app.listen(port, () => {
   console.log('Server is up!');
});