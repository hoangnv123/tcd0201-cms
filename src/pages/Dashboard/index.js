import React, { useEffect } from 'react';
import i18next from 'i18next';
// import { useSelector, useDispatch } from 'react-redux';
// import { getSummaries } from 'redux/config/actions';
import PageTitle from 'components/common/PageTitle';
import HomeWrapper from './styles';

const Home = () => {
  // const summaries = useSelector((state) => state.config.summaries);
  // const dispatch = useDispatch();
  useEffect(() => {
    // dispatch(getSummaries());
    // eslint-disable-next-line
  }, []);
  return (
    <HomeWrapper>
      <PageTitle>{i18next.t('home.title.overview')}</PageTitle>
    </HomeWrapper>
  );
};

Home.propTypes = {};

export default Home;
