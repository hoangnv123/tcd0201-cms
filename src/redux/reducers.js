import { combineReducers } from 'redux';
import auth from './auth/slice';
// import here
import users from './users/slice';
import notifications from './notifications/slice';
import reference from './referenceData/slice';
import config from './config/slice';

export default () =>
  combineReducers({
    auth,
    config,
    reference,
    // add reducer here
    users,
    notifications,
  });
